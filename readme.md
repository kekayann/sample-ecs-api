# Getting started with ECS and GitLab CI/CD

## Containierize the app
`docker build -t sample-ecs .`

## ECR
* Create a new ECR Repo and Push the docker from local

## Cluster
* Create a Networking only cluster (Fargate)
* Create Fargate launch type `Task Definition` (wrapper around our container with)
* Add the container details
* Creating the Service in our cluster for particluar task

## IAM USER

*   AmazonEC2ContainerRegistryFullAccess 
*   AmazonEC2ContainerServiceFullAccess
## Integrate `gitlab-ci`

* create varaibles in `CI/CD` settings
    - `AWS_ACCESS_KEY_ID`
    - `AWS_SECRET_ACCESS_KEY`
    - `AWS_DEFAULT_REGION`
    - `CI_AWS_ECR_REPO_URL`
    - `CI_AWS_ECS_CLUSTER`
    - `CI_AWS_ECS_SERVICE`
    - `CI_AWS_ECS_TASK_DEFINITION`
    - `AUTO_DEVOPS_PLATFORM_TARGET`

* refer the yml file 
 